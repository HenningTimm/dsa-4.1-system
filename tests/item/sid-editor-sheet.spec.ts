class MockTJSDocument {
  public doc: any
  set(doc) {
    this.doc = doc
  }
}

class MockSvelteApplication {
  close() {
    return
  }
}

class MockSidEditorSheetShellSvelte {}

// Mock the ResizeObserver
const ResizeObserverMock = vi.fn(() => ({
  observe: vi.fn(),
  unobserve: vi.fn(),
  disconnect: vi.fn(),
}))

// Stub the global ResizeObserver
vi.stubGlobal('ResizeObserver', ResizeObserverMock)

vi.mock('@typhonjs-fvtt/runtime/svelte/store/fvtt/document', () => ({
  TJSDocument: MockTJSDocument,
}))
vi.mock('@typhonjs-fvtt/runtime/svelte/application', () => ({
  SvelteApplication: MockSvelteApplication,
}))
vi.mock('../../src/ui/sid-editor/SidEditorSheetShell.svelte', () => ({
  default: MockSidEditorSheetShellSvelte,
}))

global.foundry = {
  utils: {
    mergeObject: (org, other) => other,
    getRoute: () => '',
  },
}

const { SidEditorSheet } = await import(
  '../../src/module/item/sid-editor-sheet.js'
)

describe('SidEditorSheet', () => {
  it('to return wanted default options', () => {
    const options = SidEditorSheet.defaultOptions

    expect(options).toMatchObject<any>({
      title: 'sidEditor',
      svelte: expect.objectContaining({
        class: expect.anything(),
        props: expect.anything(),
        target: document.body,
      }),
      height: 'auto',
      resizable: true,
      width: 500,
    })
  })

  it('to provide svelte store as prop', () => {
    const doc = { test: 'test' } as unknown as Item
    const sheet = new SidEditorSheet(doc)
    const options = SidEditorSheet.defaultOptions
    expect(options.svelte.props.bind(sheet)().doc.doc).toEqual(doc)
  })

  it('to provide localizer as prop', () => {
    const doc = { test: 'test' } as unknown as Item
    const sheet = new SidEditorSheet(doc)
    const options = SidEditorSheet.defaultOptions
    expect(options.svelte.props.bind(sheet)().localize('abc')).toEqual('abc')
  })
})

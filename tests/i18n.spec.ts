import { modifierLabel } from '../src/module/i18n.js'

describe('modifierLabel', () => {
  it('should return the localized label for a modifier', () => {
    const modifier = {
      name: 'Wuchtschlag',
      value: 2,
      modifierType: 'other',
    }

    const localize = (key: string) => `Localized ${key}`

    const label = modifierLabel(modifier, localize)

    expect(label).toBe('Localized Wuchtschlag')
  })

  it('should return the localized label for a modifier with class', () => {
    const modifier = {
      name: 'Wuchtschlag',
      class: 'Combat',
      mod: 2,
      modifierType: 'other',
    }

    const localize = (key: string) => `Localized ${key}`

    const label = modifierLabel(modifier, localize)

    expect(label).toBe('Localized Combat (Localized Wuchtschlag)')
  })

  it('should return the localized label for a modifier with source', () => {
    const modifier = {
      name: 'Wuchtschlag',
      source: 'Super-Effect',
      mod: 2,
      modifierType: 'other',
    }

    const localize = (key: string) => `Localized ${key}`

    const label = modifierLabel(modifier, localize)

    expect(label).toBe('Localized Wuchtschlag [Localized Super-Effect]')
  })

  it('should return the localized label for a modifier with class and source', () => {
    const modifier = {
      name: 'Wuchtschlag',
      class: 'Combat',
      source: 'Super-Effect',
      mod: 2,
      modifierType: 'other',
    }

    const localize = (key: string) => `Localized ${key}`

    const label = modifierLabel(modifier, localize)

    expect(label).toBe(
      'Localized Combat (Localized Wuchtschlag) [Localized Super-Effect]'
    )
  })
})

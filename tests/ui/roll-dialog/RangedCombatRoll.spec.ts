import { fireEvent, render, within } from '@testing-library/svelte'
import { RangeClasses, SizeClasses } from '../../../src/module/model/items.js'

import userEvent from '@testing-library/user-event'

import RangedCombatRoll from '../../../src/ui/roll-dialog/RangedCombatRoll.svelte'
import { BaseCharacter } from '../../../src/module/model/character.js'
import { ModifierDescriptor } from '../../../src/module/model/modifier.js'
import {
  CoverModifierNames,
  DarknessModifierNames,
  MovementModifierNames,
  SightModifierNames,
  SteepshotModifierNames,
  WindModifierNames,
} from '../../../src/module/model/rules/enhanced-ranged-combat.js'
import { writable } from 'svelte/store'
import Fragment from '@playpilot/svelte-fragment-component'
import html from '@playpilot/svelte-htm'

const settings = { get: vi.fn() }

vi.mock(
  '../../../src/ui/roll-dialog/Roll.svelte',
  async () => await import('./MockRoll.svelte')
)

vi.mock('../../../src/module/utils.js', async () => {
  const actual = (await vi.importActual(
    '../../../src/module/utils.js'
  )) as object
  return {
    ...actual,
    getSettings: () => settings,
  }
})

async function checkAndSetSelectOptions(label, options, option, getByText) {
  const select = within(getByText(label + ':')).getByLabelText(
    'select'
  ) as HTMLSelectElement
  expect(Array.from(select.options).map((o) => o.textContent)).toEqual(options)
  await userEvent.selectOptions(select, option)
}

async function setCheckbox(label, getByText) {
  const checkbox = within(getByText(label).parentElement).getByLabelText(
    'checkbox'
  )
  await fireEvent.click(checkbox)
}

describe('RangedCombatRoll', () => {
  const character = {
    rangedAttackModifiers: vi.fn(),
    rangedAttackDuration: vi.fn(),
  } as unknown as BaseCharacter

  test('Is rendered correctly and submits correctly for basic mode', async () => {
    const callback = vi.fn()

    settings.get.mockReturnValue(false)

    const localize = (a) => a
    const mockRoll = { submit: () => undefined }
    const context = {
      mock: writable(mockRoll),
    }

    const { getByText } = render(
      html`
    <${Fragment} context=${context}>
    <${RangedCombatRoll} callback=${callback} localize=${localize} character=${character} />
    </$>
    `
    )

    const sizeClass = 'small'
    const rangeClass = 'close'

    await checkAndSetSelectOptions(
      'sizeClass',
      SizeClasses,
      sizeClass,
      getByText
    )
    await checkAndSetSelectOptions(
      'rangeClass',
      RangeClasses,
      rangeClass,
      getByText
    )

    mockRoll.submit()

    expect(callback).toHaveBeenCalledWith(
      new Map(),
      expect.objectContaining({
        sizeClass,
        rangeClass,
      })
    )
  })

  test('Is rendered correctly and submits correctly for basic mode with size class and range class', async () => {
    const callback = vi.fn()

    settings.get.mockReturnValue(false)
    const localize = (a) => a

    const sizeClass = 'big'
    const rangeClass = 'far'

    const mockedRangedModifiers = new Map<string, ModifierDescriptor>([
      [
        'sizeClass',
        {
          name: 'sizeClass',
          mod: 2,
          modifierType: 'other',
        },
      ],
      [
        'rangeClass',
        {
          name: 'rangeClass',
          mod: 3,
          modifierType: 'other',
        },
      ],
    ])

    character.rangedAttackModifiers.mockReturnValue(mockedRangedModifiers)

    const mockRoll = { submit: () => undefined }
    const context = {
      mock: writable(mockRoll),
    }
    const { getByText } = render(
      html`
    <${Fragment} context=${context}>
    <${RangedCombatRoll} callback=${callback} localize=${localize} character=${character} />
    </$>
    `
    )

    await checkAndSetSelectOptions(
      'sizeClass',
      SizeClasses,
      sizeClass,
      getByText
    )
    await checkAndSetSelectOptions(
      'rangeClass',
      RangeClasses,
      rangeClass,
      getByText
    )

    expect(character.rangedAttackModifiers).toHaveBeenCalled()

    mockRoll.submit()

    expect(callback).toHaveBeenCalledWith(
      mockedRangedModifiers,
      expect.objectContaining({
        sizeClass,
        rangeClass,
      })
    )
  })

  const defaults = {
    sizeClass: 'small',
    rangeClass: 'close',
    darkness: 'dark',
    sight: 'dust',
    cover: 'half',
    movement: 'evasive',
    wind: 'crosswind',
    steepshot: 'upwarts',
    isBowStretched: true,
  }

  const combinations = [
    defaults,
    {
      ...defaults,
      sizeClass: 'medium',
      rangeClass: 'medium',
      isBowStretched: false,
    },
  ]

  console.log(combinations)

  test.each(combinations)(
    'Is rendered correctly and submits correctly for enhanced mode',
    async ({
      sizeClass,
      rangeClass,
      darkness,
      sight,
      cover,
      movement,
      wind,
      steepshot,
      isBowStretched,
    }) => {
      const callback = vi.fn()
      settings.get.mockReturnValue(true)

      const mockedRangedModifiers = new Map<string, ModifierDescriptor>([
        [
          'sizeClass',
          {
            name: 'sizeClass',
            mod: 2,
            modifierType: 'other',
          },
        ],
        [
          'rangeClass',
          {
            name: 'rangeClass',
            mod: 3,
            modifierType: 'other',
          },
        ],
      ])

      character.rangedAttackModifiers.mockReturnValue(mockedRangedModifiers)

      const localize = (a) => a
      const mockRoll = { submit: () => undefined }
      const context = {
        mock: writable(mockRoll),
      }
      const { getByText } = render(
        html`
      <${Fragment} context=${context}>
      <${RangedCombatRoll} callback=${callback} localize=${localize} character=${character} />
      </$>
      `
      )

      await checkAndSetSelectOptions(
        'sizeClass',
        SizeClasses,
        sizeClass,
        getByText
      )
      await checkAndSetSelectOptions(
        'rangeClass',
        RangeClasses,
        rangeClass,
        getByText
      )
      await checkAndSetSelectOptions(
        'darkness',
        DarknessModifierNames,
        darkness,
        getByText
      )
      await checkAndSetSelectOptions(
        'sight',
        SightModifierNames,
        sight,
        getByText
      )
      await checkAndSetSelectOptions(
        'cover',
        CoverModifierNames,
        cover,
        getByText
      )
      await checkAndSetSelectOptions(
        'movement',
        MovementModifierNames,
        movement,
        getByText
      )
      await checkAndSetSelectOptions('wind', WindModifierNames, wind, getByText)
      await checkAndSetSelectOptions(
        'steepshot',
        SteepshotModifierNames,
        steepshot,
        getByText
      )

      if (isBowStretched) {
        await setCheckbox('bowIsStretched', getByText)
      }
      await setCheckbox('arrowOnBow', getByText)
      await setCheckbox('axxeleratus', getByText)
      await setCheckbox('quickshot', getByText)
      await setCheckbox('secondAttackInRound', getByText)

      const frayCheckbox = within(getByText('personsInFray:')).getByLabelText(
        'checkbox'
      )
      await fireEvent.click(frayCheckbox)
      // const fraySlider = within(
      //   frayCheckbox.parentElement?.parentElement?.parentElement
      // ).getByRole('spinbutton')

      const fray = 2

      mockRoll.submit()

      expect(callback).toHaveBeenCalledWith(
        mockedRangedModifiers,
        expect.objectContaining({
          sizeClass,
          rangeClass,
          darkness,
          sight,
          cover,
          movement,
          wind,
          steepshot,
          isBowStretched,
          isArrowOnBow: true,
          usesAxxeleratus: true,
          quickshot: true,
          secondAttackInRound: true,
          fray,
        })
      )
    }
  )
})

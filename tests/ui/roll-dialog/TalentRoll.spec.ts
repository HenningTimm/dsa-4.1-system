import TalentRoll from '../../../src/ui/roll-dialog/TalentRoll.svelte'
import { render } from '@testing-library/svelte'
import userEvent from '@testing-library/user-event'

import { BaseCharacter } from '../../../src/module/model/character.js'
import { writable } from 'svelte/store'
import Fragment from '@playpilot/svelte-fragment-component'
import html from '@playpilot/svelte-htm'

vi.mock(
  '../../../src/ui/roll-dialog/Roll.svelte',
  async () => await import('./MockRoll.svelte')
)

describe('TalentRoll', () => {
  const callback = vi.fn()
  const localize = (a) => a
  const mockRoll = { submit: () => undefined }
  const context = {
    mock: writable(mockRoll),
  }
  const testAttributes = ['courage', 'courage', 'courage']

  beforeEach(() => {
    callback.mockReset()
  })

  test('Submit with effective encumbarance', async () => {
    const effectiveEncumbrance = 5
    const skill = {
      skillType: 'talent',
      talentType: 'normal',
      effectiveEncumbarance: {
        formula: 'BE',
        type: 'formula',
      },
      testAttributes,
    }
    const character = {
      effectiveEncumbarance: vi.fn().mockReturnValue(effectiveEncumbrance),
    } as unknown as BaseCharacter

    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${TalentRoll} callback=${callback} localize=${localize} character=${character} skill=${skill} />
      </$>
      `
    )
    const effectiveEncumbaranceDiv = getByTestId('effective-encumbarance')
    expect(effectiveEncumbaranceDiv).toHaveTextContent(
      'effectiveEncumbarance: BE = 5'
    )

    const effectiveEncumbranceCheckbox = getByTestId(
      'effective-encumbarance-checkbox'
    )
    // await userEvent.click(effectiveEncumbranceCheckbox)
    expect(effectiveEncumbranceCheckbox).toBeChecked()

    mockRoll.submit()

    const encumbaranceModifier = {
      name: 'effectiveEncumbarance',
      mod: effectiveEncumbrance,
      modifierType: 'other',
    }
    expect(callback).toHaveBeenCalledWith(
      new Map([['effectiveEncumbarance', encumbaranceModifier]]),
      { testAttributes }
    )
  })

  test('Submit with disabled effective encumbarance', async () => {
    const effectiveEncumbrance = 5
    const skill = {
      skillType: 'talent',
      talentType: 'normal',
      effectiveEncumbarance: {
        formula: 'BE',
        type: 'formula',
      },
      testAttributes,
    }
    const character = {
      effectiveEncumbarance: vi.fn().mockReturnValue(effectiveEncumbrance),
    } as unknown as BaseCharacter

    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${TalentRoll} callback=${callback} localize=${localize} character=${character} skill=${skill} />
      </$>
      `
    )
    const effectiveEncumbranceCheckbox = getByTestId(
      'effective-encumbarance-checkbox'
    )
    expect(effectiveEncumbranceCheckbox).toBeChecked()
    await userEvent.click(effectiveEncumbranceCheckbox)
    expect(effectiveEncumbranceCheckbox).not.toBeChecked()

    mockRoll.submit()

    expect(callback).toHaveBeenCalledWith(new Map(), { testAttributes })
  })

  test('Submit without effective encumbarance', async () => {
    const effectiveEncumbrance = 5
    const skill = {
      skillType: 'talent',
      talentType: 'normal',
      effectiveEncumbarance: {
        formula: '',
        type: 'none',
      },
      testAttributes,
    }
    const character = {
      effectiveEncumbarance: vi.fn().mockReturnValue(effectiveEncumbrance),
    } as unknown as BaseCharacter

    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${TalentRoll} callback=${callback} localize=${localize} character=${character} skill=${skill} />
      </$>
      `
    )
    const effectiveEncumbranceCheckbox = getByTestId(
      'effective-encumbarance-checkbox'
    )
    expect(effectiveEncumbranceCheckbox).toBeDisabled()

    mockRoll.submit()

    expect(callback).toHaveBeenCalledWith(new Map(), { testAttributes })
  })

  test('Use fallback to zero for missing formula', async () => {
    const effectiveEncumbrance = 5
    const skill = {
      skillType: 'talent',
      talentType: 'normal',
      effectiveEncumbarance: {
        type: 'formula',
      },
      testAttributes,
    }
    const character = {
      effectiveEncumbarance: vi.fn().mockReturnValue(effectiveEncumbrance),
    } as unknown as BaseCharacter

    render(
      html`
      <${Fragment} context=${context}>
      <${TalentRoll} callback=${callback} localize=${localize} character=${character} skill=${skill} />
      </$>
      `
    )

    mockRoll.submit()

    const encumbaranceModifier = {
      name: 'effectiveEncumbarance',
      mod: 0,
      modifierType: 'other',
    }
    expect(callback).toHaveBeenCalledWith(
      new Map([['effectiveEncumbarance', encumbaranceModifier]]),
      { testAttributes }
    )
  })
})

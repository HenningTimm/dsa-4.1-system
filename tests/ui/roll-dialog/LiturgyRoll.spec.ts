import LiturgyRoll from '../../../src/ui/roll-dialog/LiturgyRoll.svelte'
import { render } from '@testing-library/svelte'
import userEvent from '@testing-library/user-event'
import { when } from 'jest-when'

import { BaseCharacter } from '../../../src/module/model/character.js'
import { writable } from 'svelte/store'
import Fragment from '@playpilot/svelte-fragment-component'
import html from '@playpilot/svelte-htm'

vi.mock(
  '../../../src/ui/roll-dialog/Roll.svelte',
  async () => await import('./MockRoll.svelte')
)

describe('LiturgyRoll', () => {
  const callback = vi.fn()
  const localize = (a) => a
  const mockRoll = { submit: () => undefined }
  const context = {
    mock: writable(mockRoll),
  }
  const testAttributes = ['courage', 'courage', 'courage']

  beforeEach(() => {
    callback.mockReset()
  })

  test('Submit with effective encumbarance', async () => {
    const skill = {
      skillType: 'liturgy',
      degree: 'III',
      testAttributes,
    }
    const expectedModifier = 4

    const character = {
      degreeModifier: vi.fn(),
    } as unknown as BaseCharacter

    when(character.degreeModifier)
      .calledWith(skill.degree)
      .mockReturnValue(expectedModifier)

    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${LiturgyRoll} callback=${callback} localize=${localize} character=${character} skill=${skill} />
      </$>
      `
    )

    const degreeInput = getByTestId('test-degree')
    await userEvent.selectOptions(degreeInput, skill.degree)

    mockRoll.submit()

    const degreeModifier = {
      name: 'degreeModifier',
      mod: expectedModifier,
      modifierType: 'other',
    }
    expect(callback).toHaveBeenCalledWith(
      new Map([['degreeModifier', degreeModifier]]),
      { testAttributes }
    )
  })
})

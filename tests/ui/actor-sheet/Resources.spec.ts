import { render, within } from '@testing-library/svelte'

import html from '@playpilot/svelte-htm'
import Fragment from '@playpilot/svelte-fragment-component'

import { writable } from 'svelte/store'

import { getGame } from '../../../src/module/utils.js'
import { createMockRuleset } from '../../ruleset/rules/helpers.js'
import { Mock } from 'vitest'
;(getGame as Mock<any>).mockImplementation(() => ({
  ruleset: createMockRuleset(),
}))

vi.mock(
  '../../../src/ui/Dialog.svelte',
  async () => await import('../MockDialog.svelte')
)

import Resources from '../../../src/ui/actor-sheet/Resources.svelte'
import {
  ResourceData,
  resourceNames,
} from '../../../src/module/model/character-data.js'
import { fireEvent } from '@testing-library/dom'

describe('Resources', () => {
  test('Is rendered correctly', async () => {
    const resource = {
      value: 5,
      min: 0,
      max: 7,
    } as ResourceData
    const context = {
      doc: writable({
        system: {
          settings: {
            hasAstralEnergy: true,
            hasKarmicEnergy: true,
          },
        },
      }),

      openDialogMock: vi.fn(),
    }
    const resources = resourceNames.reduce(
      (a, v) => ({ ...a, [v]: resource }),
      {}
    )
    const { getAllByRole, getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${Resources} resources=${resources} />
      </$>
      `
    )

    const groups = getAllByRole('group')
    expect(
      groups.map((group) => within(group).getByTestId('label').textContent)
    ).toEqual(resourceNames)
    groups.forEach((group) =>
      expect(within(group).getByRole('spinbutton')).toHaveValue(resource.value)
    )

    const applyDamageButton = getByTestId('open-apply-damage-dialog')
    expect(applyDamageButton).toBeInTheDocument()
    await fireEvent.click(applyDamageButton)
    expect(context.openDialogMock).toHaveBeenCalled()
  })
})

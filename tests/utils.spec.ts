export {}
const { getCanvas, getGame } = await vi.importActual('../src/module/utils.js')

describe('getGame', function () {
  it('should return the global game instance', () => {
    class Game {}
    global.Game = Game
    const game = new Game()
    global.game = game
    expect(getGame()).toEqual(game)
  })

  it('should throw on missing game instance', () => {
    class Game {}
    global.Game = Game
    global.game = {}
    expect(getGame).toThrow()
  })
})

describe('getCanvas', function () {
  it('should return the global canvas instance', () => {
    class Canvas {}
    global.Canvas = Canvas
    const canvas = new Canvas()
    global.canvas = canvas
    expect(getCanvas()).toEqual(canvas)
  })

  it('should throw on missing canvas instance', () => {
    class Canvas {}
    global.Canvas = Canvas
    global.canvas = {}
    expect(getCanvas).toThrow()
  })
})

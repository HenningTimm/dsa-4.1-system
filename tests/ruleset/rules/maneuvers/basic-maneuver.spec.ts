import {
  BasicManeuverRule,
  ComputeManeuverList,
} from '../../../../src/module/ruleset/rules/maneuvers/basic-maneuver.js'

import {
  AttackAction,
  CombatActionData,
  CombatActionResult,
  DodgeAction,
  ParryAction,
} from '../../../../src/module/ruleset/rules/basic-combat.js'
import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { TestAction } from '../../test-classes.js'
import { createTestRuleset } from '../helpers.js'
import { Computation } from '../../../../src/module/ruleset/rule-components.js'
import {
  type ActionIdentifier,
  type ComputationIdentifier,
} from '../../../../src/module/model/ruleset.js'
import { RangedAttackAction } from '../../../../src/module/ruleset/rules/basic-ranged-combat.js'
import type {
  ManeuverDescriptor,
  ManeuverType,
  ModifierDescriptor,
} from '../../../../src/module/model/modifier.js'
import { Maneuver } from '../../../../src/module/character/maneuver.js'
import {
  ComputeAttack,
  ComputeParry,
  ComputeRangedAttack,
} from '../../../../src/module/ruleset/rules/derived-combat-attributes.js'
import {
  CombatComputationData,
  CombatComputationResult,
} from '../../../../src/module/model/rules/derived-combat-attributes.js'

describe('Basic Maneuver', function () {
  const ruleset = createTestRuleset()

  const executionResult = {} as any

  const executeHook = vi.fn().mockReturnValue(executionResult)

  ruleset.registerAction(new TestAction(AttackAction, executeHook))
  ruleset.registerAction(new TestAction(ParryAction, executeHook))
  ruleset.registerAction(new TestAction(DodgeAction, executeHook))
  ruleset.registerAction(new TestAction(RangedAttackAction, executeHook))

  const computeHook = vi.fn()

  ruleset.registerComputation(new Computation(ComputeAttack, computeHook))
  ruleset.registerComputation(new Computation(ComputeParry, computeHook))
  ruleset.registerComputation(new Computation(ComputeRangedAttack, computeHook))

  ruleset.add(BasicManeuverRule)
  ruleset.compileRules()

  const character = {} as BaseCharacter

  test.each([AttackAction, ParryAction])(
    'should add penality of failed maneuver to action result',
    async function (
      action: ActionIdentifier<CombatActionData, CombatActionResult>
    ) {
      const mightyStrike = new Maneuver('TestManeuver', 'offensive')
      mightyStrike.mod = 5
      const options = {
        character,
        modifiers: new Map([['mightyStrike', mightyStrike]]),
        talent: undefined,
        weapon: undefined,
        mod: 0,
      }
      executionResult.options = options
      executionResult.success = false

      const expectedPenality = 5

      const result = await ruleset.execute(action, options)

      expect(result.penality).toEqual(expectedPenality)
    }
  )

  test.each([AttackAction, ParryAction, DodgeAction, RangedAttackAction])(
    'should add penality of failed maneuver to action result',
    async function (
      action: ActionIdentifier<CombatActionData, CombatActionResult>
    ) {
      const modifiers: Map<string, ModifierDescriptor> = new Map([
        [
          'TestManeuver',
          {
            name: 'TestManeuver',
            mod: 3,
            modifierType: 'maneuver',
          },
        ],
        [
          'Test Modifier',
          {
            name: 'Test Modifier',
            mod: 4,
            modifierType: 'other',
          },
        ],
      ])
      const options = {
        character,
        modifiers,
        talent: undefined,
        weapon: undefined,
        mod: 0,
      }
      executionResult.options = options
      executionResult.success = false

      const expectedTotalMod = 7

      await ruleset.execute(action, options)

      expect(executeHook).toHaveBeenCalledWith(
        expect.objectContaining({
          mod: expectedTotalMod,
        })
      )
    }
  )

  test.each([ComputeAttack, ComputeParry, ComputeRangedAttack])(
    'should add appliable maneuvers modifiers to the modifiers',
    function (
      computeAction: ComputationIdentifier<
        CombatComputationData,
        CombatComputationResult
      >
    ) {
      character.maneuverModifiers = [
        {
          name: 'TestManeuver',
          value: 3,
          source: 'TestSource',
        },
        {
          name: 'TestManeuver2',
          value: 3,
          source: 'TestSource',
        },
      ]
      const maneuvers = [
        {
          name: 'TestManeuver',
          mod: 4,
          type: 'offensive',
          minMod: 0,
          modifierType: 'maneuver',
        },
      ] as ManeuverDescriptor[]
      const expectedModifiers = new Map<string, ModifierDescriptor>([
        [
          'TestManeuver-TestSource',
          {
            name: 'TestManeuver',
            mod: 3,
            modifierType: 'maneuverModifier',
            source: 'TestSource',
          },
        ],
      ])
      const options = {
        character,
        talent: undefined,
        weapon: undefined,
        maneuvers,
        mod: 0,
        modifiers: new Map(),
      }
      computeHook.mockReturnValueOnce({ modifiers: new Map() })
      const result = ruleset.compute(computeAction, options)

      expect(result.modifiers).toEqual(expectedModifiers)
    }
  )

  test.each<[ManeuverType]>([['offensive'], ['defensive']])(
    'should be able to compute a list of available maneuvers',
    function (maneuverType: ManeuverType) {
      const result = ruleset.compute(ComputeManeuverList, {
        character,
        maneuverType,
      })
      expect(result.maneuvers).toEqual([])
    }
  )

  afterEach(() => {
    vi.clearAllMocks()
  })
})

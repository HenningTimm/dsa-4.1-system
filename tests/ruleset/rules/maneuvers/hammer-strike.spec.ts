import { when } from 'jest-when'

import {
  HammerStrikeRule,
  HammerStrikeManeuver,
  HammerStrike,
} from '../../../../src/module/ruleset/rules/maneuvers/hammer-strike.js'

import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { Computation } from '../../../../src/module/ruleset/rule-components.js'
import { createTestRuleset } from '../helpers.js'
import { ComputeManeuverList } from '../../../../src/module/ruleset/rules/maneuvers/basic-maneuver.js'
import type { ManeuverType } from '../../../../src/module/model/modifier.js'

describe('Hammer-Strike', function () {
  const ruleset = createTestRuleset()

  ruleset.registerComputation(
    new Computation(
      ComputeManeuverList,
      vi.fn().mockImplementation(() => ({ maneuvers: [] }))
    )
  )

  ruleset.add(HammerStrikeRule)
  ruleset.compileRules()

  const character = {} as BaseCharacter

  it('should have a minimal modifier of 8', function () {
    expect(HammerStrikeManeuver.minMod).toEqual(8)
  })

  test.each([
    [false, 'offensive', false],
    [false, 'defensive', false],
    [false, 'defensive', true],
    [true, 'offensive', true],
  ] as [boolean, ManeuverType, boolean][])(
    'should add hammer strike only if the character has the ability and the list is for offensive maneuvers',
    function (
      expected: boolean,
      maneuverType: ManeuverType,
      hasAbility: boolean
    ) {
      character.has = vi.fn()
      when(character.has).calledWith(HammerStrike).mockReturnValue(hasAbility)

      const result = ruleset.compute(ComputeManeuverList, {
        character,
        maneuverType,
      })

      expect(result.maneuvers.includes(HammerStrikeManeuver)).toEqual(expected)
    }
  )
})

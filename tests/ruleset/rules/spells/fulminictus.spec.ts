import {
  Fulminictus,
  FulminictusRule,
} from '../../../../src/module/ruleset/rules/spells/fulminictus.js'
import { TestAction } from '../../test-classes.js'

import {
  SkillActionData,
  SpellAction,
} from '../../../../src/module/ruleset/rules/basic-skill.js'
import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { createTestRuleset } from '../helpers.js'

describe('Fulminictus', function () {
  const ruleset = createTestRuleset()

  const spellActionResult = {} as any
  const executeHook = vi.fn().mockReturnValue(spellActionResult)
  const skillRollAction = new TestAction(SpellAction, executeHook)
  ruleset.registerAction(skillRollAction)

  ruleset.add(FulminictusRule)
  ruleset.compileRules()

  const character = {} as BaseCharacter

  it('should add damage formula on success', async function () {
    const options: SkillActionData = {
      character,
      skill: Fulminictus,
    }
    spellActionResult.success = true
    spellActionResult.options = {}
    const rollResult = 5
    spellActionResult.roll = {
      total: rollResult,
    }
    const expectedDamage = '2d6 + 5'

    const result = await ruleset.execute(SpellAction, options)

    expect(result.damage).toEqual(expectedDamage)
  })
})

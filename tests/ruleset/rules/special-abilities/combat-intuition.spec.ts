import { when } from 'jest-when'
import {
  CombatIntuition,
  CombatIntuitionRule,
} from '../../../../src/module/ruleset/rules/special-abilities/combat-intuition.js'

import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { Computation } from '../../../../src/module/ruleset/rule-components.js'
import { createTestRuleset } from '../helpers.js'
import { ComputeInitiative } from '../../../../src/module/ruleset/rules/basic-combat.js'

describe('Combat intuition', function () {
  const ruleset = createTestRuleset()
  const initiativeValue = 10

  const executeHook = vi.fn().mockReturnValue(initiativeValue)
  ruleset.registerComputation(new Computation(ComputeInitiative, executeHook))

  ruleset.add(CombatIntuitionRule)
  ruleset.compileRules()

  const character = {} as BaseCharacter

  it('should add 2 to the initiative if the character has combat intuition', async function () {
    character.has = vi.fn()

    const options = {
      character,
    }

    when(character.has).calledWith(CombatIntuition).mockReturnValue(true)

    const result = ruleset.compute(ComputeInitiative, options)

    expect(result).toEqual(12)
  })

  it('should not change the initiative if the character has no combat intuition', async function () {
    character.has = vi.fn()

    const options = {
      character,
    }

    when(character.has).calledWith(CombatIntuition).mockReturnValue(false)

    const result = ruleset.compute(ComputeInitiative, options)

    expect(result).toEqual(10)
  })
})

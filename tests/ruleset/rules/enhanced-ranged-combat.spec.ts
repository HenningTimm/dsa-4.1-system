import { Computation } from '../../../src/module/ruleset/rule-components.js'
import {
  ComputeDamageFormula,
  ComputeRangedAttack,
} from '../../../src/module/ruleset/rules/derived-combat-attributes.js'
import {
  BasicRangedCombatRule,
  RangedAttackAction,
} from '../../../src/module/ruleset/rules/basic-ranged-combat.js'
import type { RangedCombatActionData } from '../../../src/module/ruleset/rules/basic-ranged-combat.js'
import { when } from 'jest-when'
import type { BaseCharacter } from '../../../src/module/model/character.js'
import type { RangedWeapon } from '../../../src/module/model/items.js'
import {
  RollAttributeToChatEffect,
  RollCombatAttribute,
} from '../../../src/module/ruleset/rules/basic-roll-mechanic.js'
import { createTestRuleset } from './helpers.js'
import { EffectSpy, TestAction } from '../test-classes.js'
import {
  ComputeRangedAttackDuration,
  EnhancedRangedCombatRule,
  Marksman,
  Sharpshooter,
} from '../../../src/module/ruleset/rules/enhanced-ranged-combat.js'
import {
  CoverModifierNames,
  DarknessModifierNames,
  MovementModifierNames,
  SightModifierNames,
  SteepshotModifierNames,
  WindModifierNames,
} from '../../../src/module/model/rules/enhanced-ranged-combat.js'

describe('EnhancedRangedCombatRule', function () {
  const character = {
    has: vi.fn(),
  } as BaseCharacter

  const ruleset = createTestRuleset()

  const computeHook = vi.fn().mockReturnValue({})
  ruleset.registerComputation(new Computation(ComputeRangedAttack, computeHook))
  const damageHook = vi.fn().mockReturnValue({})
  const damageComputation = new Computation(ComputeDamageFormula, damageHook)
  ruleset.registerComputation(damageComputation)
  ruleset.add(BasicRangedCombatRule)
  ruleset.add(EnhancedRangedCombatRule)
  ruleset.compileRules()
  const rangedAttackValue = 12
  const executeHook = vi.fn().mockReturnValue({})
  const rollAction = new TestAction(RollCombatAttribute, executeHook)
  ruleset.registerAction(rollAction)
  const effectSpy = new EffectSpy(RollAttributeToChatEffect)
  ruleset.registerEffect(effectSpy)

  const weapon = {} as RangedWeapon

  character.rangedAttackValue = vi.fn()
  when(character.rangedAttackValue)
    .calledWith(expect.objectContaining({ weapon }))
    .mockReturnValue(rangedAttackValue)

  let options: RangedCombatActionData

  beforeEach(function () {
    executeHook.mockClear()
    options = {
      character,
      weapon,
      rangeClass: 'close',
      sizeClass: 'big',
      darkness: 'daylight',
      sight: 'normal',
      cover: 'none',
      movement: 'slowMovement',
      wind: 'windstill',
      quickshot: false,
      secondAttackInRound: false,
      aimTime: 0,
      customModifier: 0,
      isArrowOnBow: false,
      isBowStretched: false,
      usesAxxeleratus: false,
      fray: 0,
      steepshot: 'none',
    }
  })

  const darknessModMap = {
    daylight: 0,
    dawn: 2,
    moonlight: 4,
    starlight: 6,
    dark: 8,
  }

  const sightModMap = {
    normal: 0,
    dust: 2,
    fog: 4,
    invisibleTarget: 8,
  }

  const coverModMap = {
    none: 0,
    half: 2,
    threeQuarter: 4,
  }

  const movementModMap = {
    immobile: -4,
    stationary: -2,
    slowMovement: 0,
    fastMovement: 2,
    evasive: 4,
  }

  const windModMap = {
    windstill: 0,
    crosswind: 4,
    strongCrosswind: 8,
  }

  const steepshotModMap = {
    none: 0,
    downwarts: 2,
    upwarts: 4,
  }

  function testComputedMod(value, mod, key) {
    const result = ruleset.compute(ComputeRangedAttack, {
      ...options,
      [key]: value,
    })
    expect(result.modifiers?.get(key)?.mod || 0).toEqual(mod)
  }

  async function testModOnAction(value, mod, key) {
    await ruleset.execute(RangedAttackAction, {
      ...options,
      [key]: value,
    })

    expect(executeHook).toHaveBeenCalledWith(
      expect.objectContaining({
        targetValue: rangedAttackValue,
        mod: mod,
      })
    )
  }

  const modifierMapTestCases = [
    [DarknessModifierNames, darknessModMap, 'darkness'],
    [SightModifierNames, sightModMap, 'sight'],
    [CoverModifierNames, coverModMap, 'cover'],
    [MovementModifierNames, movementModMap, 'movement'],
    [WindModifierNames, windModMap, 'wind'],
    [SteepshotModifierNames, steepshotModMap, 'steepshot'],
  ]
    .map(([levels, modMap, key]) => levels.map((level) => [level, modMap, key]))
    .flat()

  test.each(modifierMapTestCases)(
    'should provide a computation for the %s modificator to the actor',
    function (level, modMap, key) {
      testComputedMod(level, modMap[level], key)
    }
  )

  test.each(modifierMapTestCases)(
    'should apply the correct modificator based on %s mod',
    async function (level, modMap, key) {
      await testModOnAction(level, modMap[level], key)
    }
  )

  const multiplierTestCases = [
    ['quickshot', 2],
    ['secondAttackInRound', 4],
  ]

  test.each(multiplierTestCases)(
    'should provide a computation for the %s modificator to the actor',
    function (key, mod) {
      testComputedMod(true, mod, key)
    }
  )

  test.each(multiplierTestCases)(
    'should apply the correct modificator based on %s',
    async function (key, mod) {
      await testModOnAction(true, mod, key)
    }
  )

  const customTestCases = [
    ['aimTime', 0, 0],
    ['aimTime', 1, 0],
    ['aimTime', 2, -1],
    ['aimTime', 4, -2],
    ['aimTime', 8, -4],
    ['aimTime', 10, -4],
    ['fray', 0, 0],
    ['fray', 1, 2],
    ['fray', 3, 6],
  ]

  test.each(customTestCases)(
    'should provide a computation for the %s modificator to the actor',
    function (key, value, mod) {
      testComputedMod(value, mod, key)
    }
  )

  test.each(customTestCases)(
    'should apply the correct modificator based on %s',
    async function (key, value, mod) {
      await testModOnAction(value, mod, key)
    }
  )

  test('should apply bonus damage by range', async function () {
    executeHook.mockReturnValueOnce({
      success: true,
    })
    damageHook.mockReturnValueOnce({ damage: '1d6' })
    weapon.damage = '1d6'
    weapon.type = 'ranged'
    weapon.bonusDamages = {
      close: 2,
    }
    const result = await ruleset.execute(RangedAttackAction, {
      ...options,
      rangeClass: 'close',
    })

    expect(result.damage?.bonusDamage).toEqual(2)
  })

  test('should compute ranged attack duration', async function () {
    weapon.type = 'ranged'
    weapon.loadtime = 3
    const result = await ruleset.compute(ComputeRangedAttackDuration, {
      ...options,
    })

    expect(result.value).toEqual(4)
  })

  test('should compute ranged attack duration with aim time', async function () {
    weapon.type = 'ranged'
    weapon.loadtime = 3
    const result = await ruleset.compute(ComputeRangedAttackDuration, {
      ...options,
      aimTime: 3,
    })

    expect(result.value).toEqual(7)
  })

  test('should compute ranged attack duration with quickshot', async function () {
    weapon.type = 'ranged'
    weapon.loadtime = 3
    const result = await ruleset.compute(ComputeRangedAttackDuration, {
      ...options,
      quickshot: true,
    })

    expect(result.value).toEqual(3)
  })

  test('should compute ranged attack duration with arrow on bow', async function () {
    weapon.type = 'ranged'
    weapon.loadtime = 3
    const result = await ruleset.compute(ComputeRangedAttackDuration, {
      ...options,
      isArrowOnBow: true,
    })

    expect(result.value).toEqual(3)
  })

  test('should compute ranged attack duration with axxeleratus', async function () {
    weapon.type = 'ranged'
    weapon.loadtime = 3
    character.has.mockReturnValue(true)
    const result = await ruleset.compute(ComputeRangedAttackDuration, {
      ...options,
      usesAxxeleratus: true,
    })

    expect(result.value).toEqual(3)
  })

  test('should compute ranged attack duration with axxeleratus with quickload and bow', async function () {
    weapon.type = 'ranged'
    weapon.talent = 'talent-bogen'
    weapon.loadtime = 3
    character.has.mockReturnValue(false)
    const result = await ruleset.compute(ComputeRangedAttackDuration, {
      ...options,
      usesAxxeleratus: true,
    })

    expect(result.value).toEqual(3)
  })

  test.each(['talent-armbrust', 'talent-bela'])(
    'should compute ranged attack duration with axxeleratus with quickload and crossbow/bela',
    async function (talent: string) {
      weapon.type = 'ranged'
      weapon.talent = talent
      weapon.loadtime = 4
      character.has.mockReturnValue(false)
      const result = await ruleset.compute(ComputeRangedAttackDuration, {
        ...options,
        usesAxxeleratus: true,
      })

      expect(result.value).toEqual(4)
    }
  )

  test('should compute ranged attack duration when bow is stretched', async function () {
    weapon.type = 'ranged'
    weapon.loadtime = 3
    const result = await ruleset.compute(ComputeRangedAttackDuration, {
      ...options,
      isBowStretched: true,
    })

    expect(result.value).toEqual(1)
  })

  test.each([
    [2, 5],
    [3, 6],
    [4, 6],
    [8, 8],
  ])(
    'should compute ranged attack duration with custom modifier',
    async function (customModifier, expected) {
      weapon.type = 'ranged'
      weapon.loadtime = 3
      const result = await ruleset.compute(ComputeRangedAttackDuration, {
        ...options,
        customModifier,
      })

      expect(result.value).toEqual(expected)
    }
  )

  test.each([
    [2, 5],
    [3, 5],
    [4, 5],
    [8, 6],
  ])(
    'should compute ranged attack duration with custom modifier for sharpshooter',
    async function (customModifier, expected) {
      weapon.type = 'ranged'
      weapon.loadtime = 3
      when(character.has).calledWith(Sharpshooter).mockReturnValue(true)
      const result = await ruleset.compute(ComputeRangedAttackDuration, {
        ...options,
        customModifier,
      })

      expect(result.value).toEqual(expected)
    }
  )

  test.each([
    [2, 5],
    [3, 5],
    [4, 5],
    [8, 5],
  ])(
    'should compute ranged attack duration with custom modifier for sharpshooter',
    async function (customModifier, expected) {
      weapon.type = 'ranged'
      weapon.loadtime = 3
      when(character.has).calledWith(Marksman).mockReturnValue(true)
      const result = await ruleset.compute(ComputeRangedAttackDuration, {
        ...options,
        customModifier,
      })

      expect(result.value).toEqual(expected)
    }
  )
})

import { when } from 'jest-when'
import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { TestAction, TestEffect } from '../../test-classes.js'
import type { TestEffectListenedResult } from '../../test-classes.js'
import {
  RollSkill,
  RollSkillToChatEffect,
} from '../../../../src/module/ruleset/rules/basic-roll-mechanic.js'
import {
  BasicSkillRule,
  FormulaAction,
} from '../../../../src/module/ruleset/rules/basic-skill.js'
import type {
  SkillDescriptor,
  Source,
  TestAttributes,
  NamedAttribute,
  Rollable,
  Formula,
} from '../../../../src/module/model/properties.js'
import { createTestRuleset } from '../helpers.js'
import { AttributeName } from '../../../../src/module/model/character-data.js'
import { GenericFormula } from '../../../../src/module/character/skill.js'
import {
  DataAccessor,
  FormulaData,
} from '../../../../src/module/model/item-data.js'
import {
  BasicMyranorFormulaRule,
  ComputeMyranorFormulaModifier,
} from '../../../../src/module/ruleset/rules/myranor/formula.js'
import { ModifierDescriptor } from '../../../../src/module/model/modifier.js'

describe('MyranorFormulaRule', function () {
  const character = {
    data: {
      advantage: () => {},
    },
  } as unknown as BaseCharacter

  const ruleset = createTestRuleset()

  ruleset.add(BasicMyranorFormulaRule)
  ruleset.compileRules()

  const executeHook = vi.fn().mockReturnValue({})
  const skillRollAction = new TestAction(RollSkill, executeHook)
  ruleset.registerAction(skillRollAction)

  ruleset.registerEffect<TestEffectListenedResult, TestEffect>(
    new TestEffect(RollSkillToChatEffect)
  )

  ruleset.add(BasicSkillRule)
  ruleset.compileRules()

  const sourceDescriptor: SkillDescriptor = {
    name: 'Carafei',
    identifier: 'source-carafei',
    skillType: 'source',
  }

  const formulaDescriptor: SkillDescriptor = {
    name: 'Todesstrahl',
    identifier: 'formula-todesstrahl',
    skillType: 'formula',
  }

  const attributeRollMock = vi.fn()
  const courage = {
    name: 'courage',
    value: 10,
    roll: attributeRollMock,
  } as Rollable<NamedAttribute>

  const charisma = {
    name: 'charisma',
    value: 12,
    roll: attributeRollMock,
  } as Rollable<NamedAttribute>

  const source = {} as Source
  const formula = {} as Formula

  type TestAttributeData = { name: AttributeName; value: number }
  const testAttributeData: [
    TestAttributeData,
    TestAttributeData,
    TestAttributeData,
  ] = [
    {
      name: courage.name,
      value: courage.value,
    },
    {
      name: courage.name,
      value: courage.value,
    },
    {
      name: charisma.name,
      value: charisma.value,
    },
  ]
  const skillValue = 8
  const testAttributes: TestAttributes = testAttributeData.map(
    (attributeData) => attributeData.name as AttributeName
  ) as TestAttributes

  source.value = skillValue
  source.testAttributes = testAttributes

  formula.testAttributes = source.testAttributes
  formula.value = source.value

  character.source = vi.fn()
  when(character.source)
    .calledWith(sourceDescriptor.identifier)
    .mockReturnValue(source)
  character.formula = vi.fn()
  when(character.formula)
    .calledWith(formulaDescriptor.identifier)
    .mockReturnValue(formula)
  character.attribute = vi.fn()
  when(character.attribute).calledWith(courage.name).mockReturnValue(courage)
  when(character.attribute).calledWith(charisma.name).mockReturnValue(charisma)
  when(character.attribute).calledWith(charisma.name).mockReturnValue(charisma)

  it('should provide basic formula roll action', async function () {
    const mod = 5
    await ruleset.execute(FormulaAction, {
      character,
      mod,
      skill: formulaDescriptor,
    })

    expect(executeHook).toBeCalledWith(
      expect.objectContaining({
        skillName: formulaDescriptor.name,
        skillType: formulaDescriptor.skillType,
        skillValue,
        mod,
        testAttributeData,
      })
    )
  })

  it('should provide calculate the formula modifier correct', async function () {
    const character = {
      data: {
        advantage: () => {},
      },
    } as unknown as BaseCharacter
    const formulaName = 'Attributo Mut'
    const quality = 5

    const formulaData = {
      name: formulaName,
      type: 'formula',
      system: {
        sid: 'attributo-formula',
        isUniquelyOwnable: true,
        description: '',
        quality: quality,
        sourceId: 'CarafeiId',
        instructionId: 'BesselungDesGeistesId',
        parameters: {
          castTime: 'fiveActions', // 4
          target: 'oneCreature', // -1
          range: 'sevenSteps', // 3
          duration: 'leftSpellPointsHours', // 2,
          structure: 'veryDifficult', // 3
        },
      } as FormulaData,
    } as DataAccessor<'formula'>

    const formula = new GenericFormula(formulaData, character, ruleset)

    const modifiers = ruleset.compute(ComputeMyranorFormulaModifier, {
      character,
      formula,
      modifiers: new Map<string, ModifierDescriptor>(),
    })

    expect(modifiers.size).toBe(2)
    expect(modifiers.get('formulaParameterModifier')?.mod).toBe(11)
    expect(modifiers.get('formulaQuality')?.mod).toBe(quality * -1)
  })
})

export default {
  transform: {
    '^.+\\.svelte$': [
      './node_modules/svelte-jester/dist/transformer.mjs',
      {
        preprocess: './svelte.config.mjs',
        verbose: true,
      },
    ],
    '^.+\\.tsx?$': [
      'ts-jest',
      {
        diagnostics: {
          exclude: ['**'],
        },
        useESM: true,
      },
    ],
  },
  extensionsToTreatAsEsm: ['.ts', '.svelte'],
  moduleNameMapper: {
    '^(\\.{1,2}/.*)\\.js$': '$1',
  },
  moduleFileExtensions: ['js', 'svelte', 'ts'],
  // preset: 'ts-jest',
  preset: 'ts-jest/presets/default-esm',
  setupFilesAfterEnv: ['jest-extended/all', '<rootDir>/jest-setup.ts'],

  testEnvironment: 'jest-environment-jsdom',

  reporters: ['default', 'jest-junit'],
  coverageReporters: ['text', 'json-summary', 'cobertura'],
}

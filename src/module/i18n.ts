import {
  Attribute,
  CombatAttribute,
  SpellModification,
  SpellTargetClass,
  LiturgyType,
  LiturgyTargetClass,
  LiturgyCastType,
  TalentType,
  TalentCategory,
  CombatTalentCategory,
  EffectiveEncumbaranceType,
} from './enums.js'
import { RangeClasses, SizeClasses } from './model/items.js'
import { ModifierDescriptor } from './model/modifier.js'
import { Localizer, getLocalizer } from './utils.js'

const I18nMap = {
  [Attribute.Courage]: 'DSA.courage',
  [Attribute.Cleverness]: 'DSA.cleverness',
  [Attribute.Intuition]: 'DSA.intuition',
  [Attribute.Charisma]: 'DSA.charisma',
  [Attribute.Dexterity]: 'DSA.dexterity',
  [Attribute.Agility]: 'DSA.agility',
  [Attribute.Constitution]: 'DSA.constitution',
  [Attribute.Strength]: 'DSA.strength',

  [CombatAttribute.BaseAttack]: 'DSA.baseAttack',
  [CombatAttribute.BaseParry]: 'DSA.baseParry',
  [CombatAttribute.BaseRangedAttack]: 'DSA.baseRangedAttack',
  [CombatAttribute.BaseInitiative]: 'DSA.baseInitiative',
  [CombatAttribute.Dodge]: 'DSA.dodge',

  [SpellModification.spellCastTime]: 'DSA.spellCastTime',
  [SpellModification.ForceEffect]: 'DSA.forceEffect',
  [SpellModification.SaveCosts]: 'DSA.saveCosts',
  [SpellModification.MultipleTargets]: 'DSA.multipleTargets',
  [SpellModification.Range]: 'DSA.range',

  [SpellTargetClass.Object]: 'DSA.object',
  [SpellTargetClass.MultipleObjects]: 'DSA.multipleObjects',
  [SpellTargetClass.Person]: 'DSA.person',
  [SpellTargetClass.MultiplePersons]: 'DSA.multiplePersons',
  [SpellTargetClass.Zone]: 'DSA.zone',
  [SpellTargetClass.Creature]: 'DSA.creature',
  [SpellTargetClass.MultipleCreatures]: 'DSA.multipleCreatures',

  [LiturgyTargetClass.G]: 'DSA.liturgyTargetClassG',
  [LiturgyTargetClass.P]: 'DSA.liturgyTargetClassP',
  [LiturgyTargetClass.PP]: 'DSA.liturgyTargetClassPP',
  [LiturgyTargetClass.PPP]: 'DSA.liturgyTargetClassPPP',
  [LiturgyTargetClass.PPPP]: 'DSA.liturgyTargetClassPPPP',
  [LiturgyTargetClass.Z]: 'DSA.liturgyTargetClassZ',
  [LiturgyTargetClass.ZZ]: 'DSA.liturgyTargetClassZZ',
  [LiturgyTargetClass.ZZZ]: 'DSA.liturgyTargetClassZZZ',
  [LiturgyTargetClass.ZZZZ]: 'DSA.liturgyTargetClassZZZZ',

  [LiturgyCastType.BumpPrayer]: 'DSA.bumpPrayer',
  [LiturgyCastType.Prayer]: 'DSA.prayer',
  [LiturgyCastType.Devotion]: 'DSA.devotion',
  [LiturgyCastType.Ceremony]: 'DSA.ceremony',
  [LiturgyCastType.Cycle]: 'DSA.cycle',

  [LiturgyType.Basic]: 'DSA.basic',
  [LiturgyType.Special]: 'DSA.special',

  [TalentType.Basic]: 'DSA.basic',
  [TalentType.Special]: 'DSA.special',

  [TalentCategory.Combat]: 'DSA.combat',
  [TalentCategory.Physical]: 'DSA.physical',
  [TalentCategory.Social]: 'DSA.social',
  [TalentCategory.Nature]: 'DSA.nature',
  [TalentCategory.Knowledge]: 'DSA.knowledge',
  [TalentCategory.Language]: 'DSA.language',
  [TalentCategory.Crafting]: 'DSA.crafting',
  [TalentCategory.Karma]: 'DSA.karmaKnowledge',
  [TalentCategory.Gift]: 'DSA.gifts',

  [CombatTalentCategory.Melee]: 'DSA.meleeCombat',
  [CombatTalentCategory.Ranged]: 'DSA.rangedCombat',
  [CombatTalentCategory.Special]: 'DSA.special',

  [EffectiveEncumbaranceType.None]: 'DSA.none',
  [EffectiveEncumbaranceType.Special]: 'DSA.special',
  [EffectiveEncumbaranceType.Formula]: 'DSA.formula',
}

export function labeledEnumValues(enumType, useKeyValue = false) {
  const localize = getLocalizer('')
  return Object.keys(enumType)
    .filter((key) => typeof enumType[key] !== 'number')
    .map((key) => ({
      label: localize(I18nMap[enumType[key]]),
      value: useKeyValue ? key : enumType[key],
    }))
}

export type I18nMap<KeyNames extends string> = Record<KeyNames, string>

export function genericI18nMap<KeyNames extends string>(
  keys: readonly KeyNames[]
): I18nMap<KeyNames> {
  return Object.fromEntries(
    keys.map((key: KeyNames) => [key, `${key}`])
  ) as I18nMap<KeyNames>
}

export interface LabeledValue<T> {
  label: string
  value: T
}

function labeledValue<KeyNames extends string>(
  i18n: I18nMap<KeyNames>,
  key: KeyNames,
  localize: Localizer = (key) => key
): LabeledValue<string> {
  return {
    label: localize(i18n[key]),
    value: key,
  }
}

export const labeledValues = <KeyNames extends string>(
  i18n: I18nMap<KeyNames>,
  keys: readonly KeyNames[],
  localize: Localizer = (key) => key
): LabeledValue<string>[] =>
  keys.map((key) => labeledValue(i18n, key, localize))

export const RangeI18nMap = genericI18nMap(RangeClasses)

export const SizeI18nMap = genericI18nMap(SizeClasses)

export function modifierLabel(
  modifier: ModifierDescriptor,
  localize: Localizer = (key) => key
): string {
  let label = localize(modifier.name)
  if (modifier.class !== undefined && modifier.name !== modifier.class) {
    label = `${localize(modifier.class)} (${label})`
  }
  if (modifier.source !== undefined) {
    label += ` [${localize(modifier.source)}]`
  }
  return label
}

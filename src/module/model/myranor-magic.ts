export type SourceType = 'essence' | 'creatures'

export const SphereTypes = [
  'death',
  'demonic',
  'elemental',
  'nature',
  'stellar',
  'time',
] as const
export type SphereType = (typeof SphereTypes)[number]

export type FormulaDurationCategory =
  | 'instantaneousNatural'
  | 'instantaneousPermanent'
  | 'timeframe'

export type FormulaTargetCategory = 'creature' | 'object' | 'zone'

export interface FormulaParameters {
  castTime: keyof FormulaCastTimeClass
  target: keyof FormulaTargetClass
  range: keyof FormulaRangeClass
  duration: keyof FormulaDurationClass
  structure: keyof FormulaStructureClass
}

export interface FormulaInstructionStructure {
  active: boolean
  name: string
}

export type QualityDegrees = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7

export const FormulaParameterClassNames = [
  'castTime',
  'target',
  'range',
  'duration',
  'structure',
] as const

export const FormulaCastTimeClasses = [
  'oneHour',
  'ritual',
  'sixGameRounds',
  'oneGameRounds',
  'twentyActions',
  'tenActions',
  'fiveActions',
  'threeActions',
  'twoActions',
  'oneAction',
] as const
export type FormulaCastTimeClass = (typeof FormulaCastTimeClasses)[number]

export const FormulaTargetClasses = [
  'oneCreature',
  'oneObject',
  'spellValueCreatures',
  'oneStepZone',
  'spellValueObjects',
  'threeTimesSpellValueCreatures',
  'spellValueStepsZone',
  'threeTimesSpellValueObjects',
  'threeTimesSpellValueStepsZone',
  'anyNumberOfCreatures',
  'anyNumberOfObject',
  'anyZone',
] as const
export type FormulaTargetClass = (typeof FormulaTargetClasses)[number]

export const FormulaTargetsByType: Record<
  FormulaTargetCategory,
  Array<FormulaTargetClass>
> = Object.freeze({
  creature: [
    'oneCreature',
    'spellValueCreatures',
    'threeTimesSpellValueCreatures',
    'anyNumberOfCreatures',
  ] as const,
  object: [
    'oneObject',
    'spellValueObjects',
    'threeTimesSpellValueObjects',
    'anyNumberOfObject',
  ] as const,
  zone: [
    'oneStepZone',
    'spellValueStepsZone',
    'threeTimesSpellValueStepsZone',
    'anyZone',
  ] as const,
})

export const FormulaRangeClasses = [
  'self',
  'touch',
  'oneStep',
  'threeSteps',
  'sevenSteps',
  'twentyOneSteps',
  'fortyNineSteps',
  'horizon',
  'outOfSight',
]
export type FormulaRangeClass = (typeof FormulaRangeClasses)[number]

export const FormulaDurationClasses = [
  'leftSpellPointsActions',
  'fiftyActions',
  'leftSpellPointsGameRounds',
  'instantaneousNatural',
  'leftSpellPointsHours',
  'leftSpellPointsTimes8hours',
  'oneWeek',
  'oneMonth',
  'oneYear',
  'instantaneousPermanent',
] as const
export type FormulaDurationClass = (typeof FormulaDurationClasses)[number]

export const FormulaDurationByType: Record<
  FormulaDurationCategory,
  Array<FormulaDurationClass>
> = Object.freeze({
  instantaneousNatural: ['instantaneousNatural'] as const,
  instantaneousPermanent: ['instantaneousPermanent'] as const,
  timeframe: [
    'leftSpellPointsActions',
    'fiftyActions',
    'leftSpellPointsGameRounds',
    'leftSpellPointsHours',
    'leftSpellPointsTimes8hours',
    'oneWeek',
    'oneMonth',
    'oneYear',
  ] as const,
})

export const FormulaStructureClasses = [
  'extremeEasy',
  'veryEasy',
  'easy',
  'difficult',
  'veryDifficult',
  'extremeDifficult',
  'complex',
  'veryComplex',
  'extremeComplex',
] as const
export type FormulaStructureClass = (typeof FormulaStructureClasses)[number]

export async function getInstructions() {
  const systemInstructions = await game.packs
    .get('dsa-41.myranorformulainstructions')
    .getDocuments()

  const customInstructions =
    (await game.items.filter((item) => item.type === 'formulaInstruction')) ||
    []
  return []
    .concat(systemInstructions, customInstructions)
    .sort((a, b) => (a.name < b.name ? -1 : 1))
}

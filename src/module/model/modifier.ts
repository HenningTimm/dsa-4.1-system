export type ModifierType = 'maneuver' | 'maneuverModifier' | 'other'

export interface ModifierDescriptor {
  class?: string
  name: string
  mod: number
  modifierType: ModifierType
  source?: string
}

export type ModifierTable<KeyNames extends string> = Record<KeyNames, number>

export type ManeuverType = 'offensive' | 'defensive'

export interface ManeuverDescriptor extends ModifierDescriptor {
  type: ManeuverType
  minMod: number
}

export type ManeuverModifier = {
  name: string
  value: number
  source: string
}

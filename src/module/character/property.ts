import { BaseCharacter } from '../model/character.js'
import { DataAccessor, PropertyItemType } from '../model/item-data.js'
import { BaseProperty } from '../model/properties.js'

export abstract class Property<ItemType extends PropertyItemType>
  implements BaseProperty
{
  public item: DataAccessor<ItemType>
  protected character: BaseCharacter

  constructor(item: DataAccessor<ItemType>, character: BaseCharacter) {
    this.item = item
    this.character = character
  }

  get name(): string {
    return this.item.name || ''
  }

  get identifier(): string {
    return this.item.system.sid
  }
}

import { AttributeName } from '../model/character-data.js'
import { BaseCharacter } from '../model/character.js'
import { DataAccessor } from '../model/item-data.js'
import {
  GeneralNamedAttribute,
  NamedAttribute,
  Rollable,
} from '../model/properties.js'
import { RollAttribute } from '../ruleset/rules/basic-roll-mechanic.js'
import type { Ruleset } from '../ruleset/ruleset.js'

export class Attribute implements Rollable<NamedAttribute> {
  private character: BaseCharacter
  private ruleset: Ruleset
  name: AttributeName

  constructor(
    character: BaseCharacter,
    name: AttributeName,
    ruleset?: Ruleset
  ) {
    this.character = character
    this.name = name
    if (ruleset) {
      this.ruleset = ruleset
    } else {
      this.ruleset = (<any>game).ruleset
    }
  }

  get value(): number {
    return this.character.data.system.base.basicAttributes[this.name].value
  }

  roll(options: any): void {
    this.ruleset.execute(RollAttribute, {
      character: this.character,
      attributeName: this.name,
      targetValue: this.value,
      ...options,
    })
  }
}

export class NegativeAttribute implements Rollable<GeneralNamedAttribute> {
  private disadvantage: DataAccessor<'disadvantage'>
  private ruleset: Ruleset

  constructor(disadvantage: DataAccessor<'disadvantage'>, ruleset?: Ruleset) {
    this.disadvantage = disadvantage
    if (ruleset) {
      this.ruleset = ruleset
    } else {
      this.ruleset = (<any>game).ruleset
    }
  }

  get value(): number {
    return this.disadvantage.system.value || 0
  }

  get name(): string {
    return this.disadvantage.name || ''
  }

  roll(options: any): void {
    this.ruleset.execute(RollAttribute, {
      attributeName: this.name,
      targetValue: this.value,
      isLocalized: true,
      ...options,
    })
  }
}

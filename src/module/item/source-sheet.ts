import { DsaItemSheet } from './item-sheet.js'
import { SourceType, SphereTypes } from '../model/myranor-magic.js'

export class SourceSheet extends DsaItemSheet {
  activateListeners(html) {
    super.activateListeners(html)
  }

  async getData() {
    const data = await super.getData()

    const sourceTypes: SourceType[] = ['essence', 'creatures']

    data.spheres = SphereTypes
    data.sourceTypes = sourceTypes

    return data
  }
}

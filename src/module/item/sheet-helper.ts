import { labeledEnumValues } from '../i18n.js'
import { getGame } from '../utils.js'

function updateOptions(html, item, attributeName) {
  const options = [...html.find('input')].reduce(
    (options, checkbox) =>
      checkbox.checked ? [...options, checkbox.name] : options,
    []
  )

  item.update({ data: { [attributeName]: options } })
}

export async function showOptionsDialog(enumType, item, attributeName) {
  const template = 'systems/dsa-41/templates/options-dialog.html'
  const options = labeledEnumValues(enumType).map((labeledValue) => ({
    ...labeledValue,
    selected: item.system[attributeName].includes(labeledValue.value),
  }))

  const html = await renderTemplate(template, { options })

  return new Promise((resolve) => {
    const game = getGame()
    new Dialog({
      title: `${game.i18n.localize('DSA.choose')} ${game.i18n.localize(
        'DSA.' + attributeName
      )}`,
      content: html,
      buttons: {
        normal: {
          label: game.i18n.localize('DSA.select'),
          callback: (html) => updateOptions(html, item, attributeName),
        },
      },
      default: 'normal',
      close: () => resolve(false),
    }).render(true)
  })
}

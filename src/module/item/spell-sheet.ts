import { ItemWithVariantsSheet } from './item-with-variants-sheet.js'
import { SpellModification, SpellTargetClass, RuleBook } from '../enums.js'
import { labeledEnumValues } from '../i18n.js'
import { createOpenPDFPageListener } from '../pdf-integration.js'
import { showOptionsDialog } from './sheet-helper.js'

export class SpellSheet extends ItemWithVariantsSheet {
  static get variantCompendium() {
    return 'world.spellvariants'
  }

  static get variantItemType() {
    return 'spellVariant'
  }

  static get variantHTMLSelector() {
    return '.spell-variants'
  }

  activateListeners(html) {
    super.activateListeners(html)

    html.find('.modifications-list .item-create').click(() => {
      showOptionsDialog(SpellModification, this.item, 'modifications')
    })

    html.find('.target-classes-list .item-create').click(() => {
      showOptionsDialog(SpellTargetClass, this.item, 'targetClasses')
    })

    html
      .find('.open-lcd-page')
      .click(createOpenPDFPageListener(RuleBook.LiberCantionesDeluxe))
  }

  async getData() {
    const data = await super.getData()

    data.data.system.modifications = labeledEnumValues(
      SpellModification
    ).filter((mod) => data.data.system.modifications.includes(mod.value))

    data.targetClasses = labeledEnumValues(SpellTargetClass).filter(
      (target_class) =>
        data.data.system.targetClasses.includes(target_class.value)
    )

    return data
  }
}

ItemWithVariantsSheet.registerSheet('spell', SpellSheet)

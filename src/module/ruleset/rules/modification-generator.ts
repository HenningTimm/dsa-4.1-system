import type { ModifierDescriptor, ModifierTable } from '../../model/modifier.js'
import { totalModifier } from './basic-roll-mechanic.js'
import type {
  CombatComputationResult,
  RangedCombatComputationData,
  RangedCombatOptionData,
} from './derived-combat-attributes.js'

interface ModifierCalculator {
  (options: RangedCombatComputationData): number
}

interface ModifierPredicate {
  (options: RangedCombatComputationData): boolean
}

interface ModifierNameCalculator {
  (options: RangedCombatComputationData): string
}

export function modificationGenerator(
  modifierCalculator: ModifierCalculator,
  modifierPredicate: ModifierPredicate,
  modifierName: string,
  modifierKey: string
)
export function modificationGenerator(
  modifierCalculator: ModifierCalculator,
  modifierPredicate: ModifierPredicate,
  ModifierNameCalculator: ModifierNameCalculator,
  modifierKey: string
)
export function modificationGenerator(
  modifierCalculator: ModifierCalculator,
  modifierPredicate: ModifierPredicate,
  modifierNameCalcOrString: string | ModifierNameCalculator,
  modifierKey: string
) {
  function hook(
    options: RangedCombatComputationData
  ): RangedCombatComputationData
  function hook(
    options: RangedCombatComputationData,
    result: CombatComputationResult
  ): CombatComputationResult
  function hook(
    options: RangedCombatComputationData,
    result?: CombatComputationResult
  ) {
    if (options.modifiers === undefined) {
      options.modifiers = new Map<string, ModifierDescriptor>()
    }
    const modifierNameCalculator: ModifierNameCalculator =
      typeof modifierNameCalcOrString === 'string'
        ? () => modifierNameCalcOrString
        : modifierNameCalcOrString
    if (modifierPredicate(options)) {
      options.modifiers.set(modifierKey, {
        name: modifierNameCalculator(options),
        mod: modifierCalculator(options),
        class: modifierKey,
        modifierType: 'other',
      })
    }
    const totalMod = totalModifier(options.modifiers)
    if (result) {
      result.modifiers = options.modifiers
      return result
    }
    options.mod = totalMod
    return options
  }
  return hook
}

export function generateModificationFromBoolean(
  optionKey: keyof RangedCombatOptionData,
  modifier: number
) {
  return modificationGenerator(
    (_options) => modifier,
    (options) => options[optionKey] === true,
    optionKey,
    optionKey
  )
}

export function generateModificationFromMultiplier(
  optionKey: keyof RangedCombatOptionData,
  baseModifier: number,
  minCap?: number
) {
  return modificationGenerator(
    (options) => {
      const multiplier = options[optionKey]

      if (multiplier && typeof multiplier === 'number') {
        let modifier = Math.ceil(baseModifier * multiplier)
        if (minCap) {
          modifier = Math.max(minCap, modifier)
        }
        return modifier
      }
      return 0
    },
    (options) => options[optionKey] !== undefined,
    optionKey,
    optionKey
  )
}

export function generateModificationFromTable<KeyNames extends string>(
  table: ModifierTable<KeyNames>,
  optionKey: keyof RangedCombatOptionData
) {
  return modificationGenerator(
    (options) => table[options[optionKey] as KeyNames],
    (options) =>
      options[optionKey] !== undefined &&
      table[options[optionKey] as KeyNames] !== undefined,
    (options) => `${options[optionKey]}`,
    optionKey
  )
}

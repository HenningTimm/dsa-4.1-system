import { CombatAction, IsSuccess } from './basic-combat.js'
import type { AttackActionResult, CombatActionData } from './basic-combat.js'

import type { Ruleset } from '../ruleset.js'
import {
  ComputeDamageFormula,
  ComputeRangedAttack,
} from './derived-combat-attributes.js'
import { RollAttributeToChatEffect } from './basic-roll-mechanic.js'
import { DescribeRule } from '../rule.js'
import { generateModificationFromTable } from './modification-generator.js'
import type { ModifierTable } from '../../model/modifier.js'
import type { RangeClass, SizeClass } from '../../model/items.js'
import { RangedCombatOptionData } from '../../model/rules/derived-combat-attributes.js'
import { CreateActionIdentifier } from '../rule-components.js'

const RangeModifiers: ModifierTable<RangeClass> = {
  veryClose: -2,
  close: 0,
  medium: 4,
  far: 8,
  veryFar: 12,
}

const SizeModifiers: ModifierTable<SizeClass> = {
  tiny: 8,
  verySmall: 6,
  small: 4,
  medium: 2,
  big: 0,
  veryBig: -2,
}

const RangeModification = generateModificationFromTable(
  RangeModifiers,
  'rangeClass'
)

const SizeModification = generateModificationFromTable(
  SizeModifiers,
  'sizeClass'
)

export interface RangedCombatActionData
  extends CombatActionData,
    RangedCombatOptionData {}

export const RangedAttackAction = CreateActionIdentifier<
  RangedCombatActionData,
  AttackActionResult<RangedCombatActionData>
>('rangedAttack')

export const BasicRangedCombatRule = DescribeRule(
  'basic-ranged-combat-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset.after(ComputeRangedAttack).do(RangeModification)
    ruleset.after(ComputeRangedAttack).do(SizeModification)
    ruleset.on(RangedAttackAction).do(CombatAction)

    ruleset.after(RangedAttackAction).trigger(RollAttributeToChatEffect)
    ruleset.before(RangedAttackAction).do(RangeModification)
    ruleset.before(RangedAttackAction).do(SizeModification)

    ruleset
      .after(RangedAttackAction)
      .when(IsSuccess)
      .do((_options, result) => ({
        ...result,
        damage: ruleset.compute(ComputeDamageFormula, {
          character: result.options.character,
          weapon: result.options.weapon,
          talent: result.options.talent,
          modifiers: result.options.modifiers,
          bonusDamage: result.bonusDamage,
          damageMultiplier: result.damageMultiplier,
        }),
      }))
  }
)

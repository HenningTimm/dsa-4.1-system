import { GlasscannonRule } from './glasscannon.js'
import { UnshakeableRule } from './unshakeable.js'

export const DisAdvantageRules = [GlasscannonRule, UnshakeableRule]

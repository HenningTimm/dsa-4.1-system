import type { BaseProperty } from '../../../model/properties.js'
import { DescribeRule } from '../../rule.js'
import { Ruleset } from '../../ruleset.js'
import type { ComputeInitiativeFormulaData } from '../basic-combat.js'
import { ComputeWoundThresholds } from '../derived-attributes.js'
import { CharacterHas } from '../maneuvers/basic-maneuver.js'

export const Unshakeable: BaseProperty = {
  name: 'Eisern',
  identifier: 'advantage-eisern',
}
export const UnshakeableRule = DescribeRule(
  'unshakeable',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .before(ComputeWoundThresholds)
      .when(CharacterHas(Unshakeable))
      .do((options: ComputeInitiativeFormulaData) => ({
        ...options,
        mod: (options.mod || 0) + 2,
      }))
  }
)

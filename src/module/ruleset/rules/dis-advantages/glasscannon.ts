import type { BaseProperty } from '../../../model/properties.js'
import { DescribeRule } from '../../rule.js'
import { Ruleset } from '../../ruleset.js'
import type { ComputeInitiativeFormulaData } from '../basic-combat.js'
import { ComputeWoundThresholds } from '../derived-attributes.js'
import { CharacterHas } from '../maneuvers/basic-maneuver.js'

export const Glasscannon: BaseProperty = {
  name: 'Glasknochen',
  identifier: 'disadvantage-glasknochen',
}
export const GlasscannonRule = DescribeRule(
  'glasscannon',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .before(ComputeWoundThresholds)
      .when(CharacterHas(Glasscannon))
      .do((options: ComputeInitiativeFormulaData) => ({
        ...options,
        mod: (options.mod || 0) - 2,
      }))
  }
)

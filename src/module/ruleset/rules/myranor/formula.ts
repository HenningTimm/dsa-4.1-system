import { CreateComputationIdentifier } from '../../rule-components.js'
import { GenericFormula } from '../../../character/skill.js'

import { DescribeRule } from '../../rule.js'
import { Ruleset } from '../../ruleset.js'
import {
  FormulaCastTimeClass,
  FormulaDurationClass,
  FormulaParameterClassNames,
  FormulaRangeClass,
  FormulaStructureClass,
  FormulaTargetClass,
  SphereType,
} from '../../../model/myranor-magic.js'
import { ModifierDescriptor, ModifierTable } from '../../../model/modifier.js'
import { SkillActionData } from '../basic-skill.js'
import { Advantage } from '../../../model/properties.js'
import { BaseComputationOptionData } from '../../../model/ruleset.js'

const FormulaCastTimeModificators: ModifierTable<FormulaCastTimeClass> = {
  oneHour: -1,
  ritual: 0,
  sixGameRounds: 0,
  oneGameRounds: 1,
  twentyActions: 2,
  tenActions: 3,
  fiveActions: 4,
  threeActions: 5,
  twoActions: 6,
  oneAction: 7,
}

const FormulaRangeModificators: ModifierTable<FormulaRangeClass> = {
  self: -1,
  touch: 0,
  oneStep: 1,
  threeSteps: 2,
  sevenSteps: 3,
  twentyOneSteps: 4,
  fortyNineSteps: 5,
  horizon: 6,
  outOfSight: 7,
}

const FormulaTargetModificators: ModifierTable<FormulaTargetClass> = {
  oneCreature: -1,
  oneObject: 0,
  spellValueCreatures: 1,
  oneStepZone: 1,
  spellValueObjects: 1,
  threeTimesSpellValueCreatures: 2,
  spellValueStepsZone: 3,
  threeTimesSpellValueObjects: 4,
  threeTimesSpellValueStepsZone: 5,
  anyNumberOfCreatures: 6,
  anyNumberOfObject: 6,
  anyZone: 7,
}

const FormulaDurationModificators: ModifierTable<FormulaDurationClass> = {
  leftSpellPointsActions: -1,
  fiftyActions: 0,
  leftSpellPointsGameRounds: 1,
  instantaneousNatural: 1,
  leftSpellPointsHours: 2,
  leftSpellPointsTimes8hours: 3,
  oneWeek: 4,
  oneMonth: 5,
  oneYear: 6,
  instantaneousPermanent: 7,
}

const FormulaStructureModificators: ModifierTable<FormulaStructureClass> = {
  extremeEasy: -1,
  veryEasy: 0,
  easy: 1,
  difficult: 2,
  veryDifficult: 3,
  extremeDifficult: 4,
  complex: 5,
  veryComplex: 6,
  extremeComplex: 7,
}

export const FormulaModifications = {
  castTime: FormulaCastTimeModificators,
  target: FormulaTargetModificators,
  range: FormulaRangeModificators,
  duration: FormulaDurationModificators,
  structure: FormulaStructureModificators,
} as const

interface MyranorFormulaModifierOptionData extends BaseComputationOptionData {
  formula: GenericFormula
  modifiers: Map<string, ModifierDescriptor>
}

export const ComputeMyranorFormulaModifier = CreateComputationIdentifier<
  MyranorFormulaModifierOptionData,
  Map<string, ModifierDescriptor>
>('myranorFormulaMod')

export const AuraConjunctionMap: Record<SphereType, object> = {
  demonic: {
    name: 'Aurakonjunktion (Dämonisch)',
    identifier: 'advantage-auraconjunction-demonic',
  },
  elemental: {
    name: 'Aurakonjunktion (Elementare)',
    identifier: 'advantage-auraconjunction-elemental',
  },
  death: {
    name: 'Aurakonjunktion (Totenwesen)',
    identifier: 'advantage-auraconjunction-death',
  },
  nature: {
    name: 'Aurakonjunktion (Naturwesen)',
    identifier: 'advantage-auraconjunction-nature',
  },
  stellar: {
    name: 'Aurakonjunktion (Stellare)',
    identifier: 'advantage-auraconjunction-stellar',
  },
  time: {
    name: 'Aurakonjunktion (Zeit)',
    identifier: 'advantage-auraconjunction-time',
  },
}

export function GetModifiersTotal(formulaParameters) {
  return FormulaParameterClassNames.map(
    (parameterClass) =>
      FormulaModifications[parameterClass][formulaParameters[parameterClass]]
  ).reduce((a, b) => a + b)
}

export function CharacterHasConjunctionToSphere(
  advantageSphere: SphereType,
  advantage: Advantage
) {
  return (options: SkillActionData | MyranorFormulaModifierOptionData) => {
    const value =
      options.character.data.advantage(advantage.identifier)?.system.value || 0
    if (value < 2) {
      return false
    }

    const formula = options.formula || options.skill
    const formulaSphere = formula.source?.sphere

    return formulaSphere === advantageSphere
  }
}

export const BasicMyranorFormulaRule = DescribeRule(
  'myranor-formula-modifier-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    const calculateMods = (options: MyranorFormulaModifierOptionData) => {
      const formulaItem = options.formula.item
      const formulaParameterModValues = FormulaParameterClassNames.map(
        (parameterClass) => {
          const parameterValue = formulaItem.system.parameters[parameterClass]
          return FormulaModifications[parameterClass][parameterValue]
        }
      )

      const formulaParameterModifier = formulaParameterModValues.reduce(
        (a, b) => a + b,
        0
      )

      options.modifiers.set('formulaParameterModifier', {
        name: 'formulaParameterModifier',
        mod: formulaParameterModifier,
        modifierType: 'other',
      })

      options.modifiers.set('formulaQuality', {
        name: 'formulaQuality',
        mod: -1 * options.formula.quality,
        modifierType: 'other',
      })

      return options.modifiers
    }

    ruleset.on(ComputeMyranorFormulaModifier).do(calculateMods)

    for (const [advantageSphere, advantage] of Object.entries(
      AuraConjunctionMap
    )) {
      ruleset
        .before(ComputeMyranorFormulaModifier)
        .when(CharacterHasConjunctionToSphere(advantageSphere, advantage))
        .do((options: MyranorFormulaModifierOptionData) => {
          options.modifiers.set('auraConjunction', {
            name: 'auraConjunction',
            mod: -3,
            modifierType: 'other',
          })

          return options
        })
    }
  }
)

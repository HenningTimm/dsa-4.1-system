import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'

import { createManeuverRule } from './maneuver-creator.js'

export const ShieldSplitter: SpecialAbility = {
  name: 'Schildspalter',
  identifier: 'ability-schildspalter',
}

export const ShieldSplitterManeuver = new Maneuver(
  'shieldSplitter',
  'offensive',
  {
    minMod: 0,
  }
)
export const ShieldSplitterRule = createManeuverRule(
  ShieldSplitter,
  ShieldSplitterManeuver
)

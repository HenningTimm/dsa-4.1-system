import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'

import { createManeuverRule } from './maneuver-creator.js'

export const DeathBlow: SpecialAbility = {
  name: 'Todesstoß',
  identifier: 'ability-todesstoss',
}

export const DeathBlowManeuver = new Maneuver('deathBlow', 'offensive', {
  minMod: 8,
})
export const DeathBlowRule = createManeuverRule(DeathBlow, DeathBlowManeuver)

import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'

import { createManeuverRule } from './maneuver-creator.js'

export const DefensiveStyle: SpecialAbility = {
  name: 'Defensiver Kampfstile',
  identifier: 'ability-defensiver-kampfstil',
}

export const DefensiveStyleManeuver = new Maneuver(
  'defensiveStyle',
  'defensive'
)

export const DefensiveStyleRule = createManeuverRule(
  DefensiveStyle,
  DefensiveStyleManeuver
)

import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'

import { createManeuverRule } from './maneuver-creator.js'

export const Sally: SpecialAbility = {
  name: 'Ausfall',
  identifier: 'ability-ausfall',
}

export const SallyManeuver = new Maneuver('sally', 'offensive')
export const SallyRule = createManeuverRule(Sally, SallyManeuver)
